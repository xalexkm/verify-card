export const removeAllButNumbers = (value) => {
    return value.replace(/\D+/g, "");
}