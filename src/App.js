import "./App.scss";
import { Alert, Col, Container, Form, Row } from "react-bootstrap";
import { Component } from "react";
import { Button } from "react-bootstrap";

import card_chip from "./assets/chip.png";

import visa from "payment-icons/min/flat/visa.svg";
import mastercard from "payment-icons/min/flat/mastercard.svg";
import american_express from "payment-icons/min/flat/amex.svg";
import maestro from "payment-icons/min/flat/maestro.svg";
import discover from "payment-icons/min/flat/discover.svg";
import unionpay from "payment-icons/min/flat/unionpay.svg";
import mir from "payment-icons/min/flat/paypal.svg";
import elo from "payment-icons/min/flat/paypal.svg";
import hiper from "payment-icons/min/flat/hipercard.svg";
import hipercard from "payment-icons/min/flat/hipercard.svg";
import diners_club from "payment-icons/min/flat/diners.svg";
import jcb from "payment-icons/min/flat/jcb.svg";

import { removeAllButNumbers } from "./services/TextTransformationService";

const cardValidator = require("card-validator");
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cardnumber1: "",
      cardnumber2: "",
      cardnumber3: "",
      cardnumber4: "",
      fullNumber: "",
      card_name: "",
      card_expiry: "",
      card_ccv: "",
      valid: undefined,
      card_name_valid: undefined,
      card_expiry_valid: undefined,
      card_ccv_valid: undefined,
      potentiallyValid: undefined,
      errorMessage: "Your card details are invalid",
      card_type: "",
      submitted: false,
      thanks_modal: false,
    };
  }

  cardNumberElement1;
  cardNumberElement2;
  cardNumberElement3;
  cardNumberElement4;
  cardNameElement;
  cardExpDateElement;
  cardCCVElement;

  errorMessageObject = {
    number: "The card number is invalid",
    name: "You have not entered you name",
    expiry: "Your expiry date is invalid",
    ccv: "Your CCV number is invalid",
  };

  componentDidMount() {
    this.cardNumberElement1 = document.getElementById("cardnumber1");
    this.cardNumberElement2 = document.getElementById("cardnumber2");
    this.cardNumberElement3 = document.getElementById("cardnumber3");
    this.cardNumberElement4 = document.getElementById("cardnumber4");
    this.cardNameElement = document.getElementById("card_name");
    this.cardExpDateElement = document.getElementById("card_exp_date");
    this.cardCCVElement = document.getElementById("card_ccv");

    for (let i = 1; i < 4; i++) {
      this["cardNumberElement" + i].addEventListener("paste", (event) => {
        let paste = (event.clipboardData || window.clipboardData).getData(
          "text"
        );
        this.setState({
          cardnumber1: removeAllButNumbers(paste.substring(0, 4)),
          cardnumber2: removeAllButNumbers(paste.substring(4, 8)),
          cardnumber3: removeAllButNumbers(paste.substring(8, 12)),
          cardnumber4: removeAllButNumbers(paste.substring(12, 16)),
        });

        event.preventDefault();
      });
    }
  }

  componentDidUpdate() {
    let fullNumber =
      this.state.cardnumber1 +
      this.state.cardnumber2 +
      this.state.cardnumber3 +
      this.state.cardnumber4;
    if (fullNumber !== this.state.fullNumber) {
      this.setState({
        fullNumber,
        card_type: cardValidator.number(fullNumber).card?.type,
        valid: cardValidator.number(this.state.fullNumber).isValid,
      });
    }
  }

  // card_name_valid: this.state.card_name !== "",
  //       card_expiry_valid: this.state.card_expiry.length === 5,
  //       card_ccv_valid: this.state.card_ccv.length === 3,

  submitCard() {
    if (
      this.state.potentiallyValid &&
      this.state.card_name_valid &&
      this.state.card_ccv_valid &&
      this.state.card_expiry_valid
    ) {
      this.setState({
        submitted: true,
      });
      setTimeout(() => {
        this.setState({
          thanks_modal: true,
        });
      }, 7000);
      return;
    }
    this.setState({
      potentiallyValid: false,
      card_ccv_valid: this.state.card_ccv.length === 3,
      card_expiry_valid: this.state.card_expiry.length === 5,
      card_name_valid: this.state.card_name !== "",
    });
  }

  cardtypes = {
    visa: visa,
    mastercard: mastercard,
    american_express: american_express,
    diners_club: diners_club,
    discover: discover,
    jcb: jcb,
    unionpay: unionpay,
    maestro: maestro,
    mir: mir,
    elo: elo,
    hiper: hiper,
    hipercard: hipercard,
  };

  render() {
    return (
      <>
        <Container className="vrf--relative">
          {this.state.thanks_modal ? (
            <div className="vrf--thanks-modal">
              Thanks for using card verifier!
            </div>
          ) : (
            ""
          )}
          {this.state.submitted && !this.state.thanks_modal ? (
            <>
              <div className="vrf--verify-spinner">
                <svg id="check" viewBox="0 0 100 100">
                  <circle
                    id="circle"
                    cx="50"
                    cy="50"
                    r="45"
                    fill="transparent"
                  />

                  <polyline
                    id="tick"
                    points="24,45 47,70 74,25"
                    fill="transparent"
                  />
                </svg>
              </div>
            </>
          ) : (
            ""
          )}
          <div
            className="vrf--card mt-3"
            style={{
              opacity: this.state.submitted ? 0.2 : 1,
              pointerEvents: this.state.submitted ? "none" : "auto",
            }}
          >
            <Row className="justify-content-end">
              <Col className="col-3">
                <div className="vrf--card-type">
                  {this.state.card_type ? (
                    <img
                      src={this.cardtypes[this.state.card_type]}
                      alt="credit card type"
                    ></img>
                  ) : (
                    ""
                  )}
                </div>
              </Col>
            </Row>

            <Row className="justify-content-start align-items-center">
              <Col sm={3}>
                <img src={card_chip} alt="credit card chip" />
              </Col>

              {/* Validation alert section of the code */}

              {/* <Col sm={9} className="vrf--validion-alert">
                      {this.state.potentiallyValid &&
                      (this.state.card_ccv_valid ||
                        this.state.card_expiry_valid ||
                        this.state.card_name_valid) ? (
                        <Alert variant="danger">
                          {this.state.errorMessage}
                        </Alert>
                      ) : (
                        ""
                      )}
                    </Col> */}
            </Row>
            <Row className="justify-content-center">
              {/* Card first number section input field logic */}

              <Col className="col-3">
                <Form.Control
                  id="cardnumber1"
                  value={this.state.cardnumber1}
                  onChange={(event) => {
                    if (event.target.value.length === 4) {
                      this.cardNumberElement2.focus();
                    }
                    this.setState({
                      cardnumber1: removeAllButNumbers(event.target.value),
                      potentiallyValid: cardValidator.number(
                        this.state.fullNumber
                      ).isPotentiallyValid,
                    });
                  }}
                  className={`text-center ${
                    this.state.potentiallyValid ||
                    this.state.potentiallyValid === undefined
                      ? "vrf--card-number"
                      : "vrf--card-number__invalid"
                  }`}
                  maxLength={4}
                  placeholder="1234"
                ></Form.Control>
              </Col>

              {/* Card second number section input field logic */}

              <Col className="col-3">
                <Form.Control
                  id="cardnumber2"
                  value={this.state.cardnumber2}
                  onChange={(event) => {
                    if (event.target.value.length === 4) {
                      this.cardNumberElement3.focus();
                    }
                    this.setState({
                      cardnumber2: removeAllButNumbers(event.target.value),
                      potentiallyValid: cardValidator.number(
                        this.state.fullNumber
                      ).isPotentiallyValid,
                    });
                  }}
                  className={`text-center ${
                    this.state.potentiallyValid ||
                    this.state.potentiallyValid === undefined
                      ? "vrf--card-number"
                      : "vrf--card-number__invalid"
                  }`}
                  maxLength={4}
                  placeholder="1234"
                ></Form.Control>
              </Col>

              {/* Card third number section input field logic */}

              <Col className="col-3">
                <Form.Control
                  id="cardnumber3"
                  value={this.state.cardnumber3}
                  onChange={(event) => {
                    if (event.target.value.length === 4) {
                      this.cardNumberElement4.focus();
                    }
                    this.setState({
                      cardnumber3: removeAllButNumbers(event.target.value),
                      potentiallyValid: cardValidator.number(
                        this.state.fullNumber
                      ).isPotentiallyValid,
                    });
                  }}
                  className={`text-center ${
                    this.state.potentiallyValid ||
                    this.state.potentiallyValid === undefined
                      ? "vrf--card-number"
                      : "vrf--card-number__invalid"
                  }`}
                  maxLength={4}
                  placeholder="1234"
                ></Form.Control>
              </Col>

              {/* Card fourth number section input field logic */}

              <Col className="col-3">
                <Form.Control
                  id="cardnumber4"
                  value={this.state.cardnumber4}
                  onChange={(event) => {
                    if (event.target.value.length === 4) {
                      this.cardExpDateElement.focus();
                    }
                    this.setState({
                      cardnumber4: removeAllButNumbers(event.target.value),
                      potentiallyValid: cardValidator.number(
                        this.state.fullNumber
                      ).isPotentiallyValid,
                    });
                  }}
                  className={`text-center ${
                    this.state.potentiallyValid ||
                    this.state.potentiallyValid === undefined
                      ? "vrf--card-number"
                      : "vrf--card-number__invalid"
                  }`}
                  maxLength={4}
                  placeholder="1234"
                ></Form.Control>
              </Col>
            </Row>
            <Row className="justify-content-between">
              {/* Card owner name input field logic */}

              <Col className="col-6">
                <Form.Control
                  id="card_name"
                  value={this.state.card_name}
                  className={`text-center ${
                    this.state.card_name_valid ||
                    this.state.card_name_valid === undefined
                      ? "vrf--card-number"
                      : "vrf--card-number__invalid"
                  }`}
                  placeholder="Full name"
                  onChange={(event) => {
                    this.setState({
                      card_name: event.target.value,
                      card_name_valid: event.target.value !== "",
                    });
                  }}
                ></Form.Control>
              </Col>

              {/* Card expiry date input field logic */}

              <Col className="col-3">
                <Form.Control
                  id="card_exp_date"
                  value={this.state.card_expiry}
                  className={`text-center ${
                    this.state.card_expiry_valid ||
                    this.state.card_expiry_valid === undefined
                      ? "vrf--card-number"
                      : "vrf--card-number__invalid"
                  }`}
                  data-testid="expiry-input"
                  maxLength={5}
                  placeholder="Expiry"
                  onChange={(event) => {
                    if (event.target.value.length === 5) {
                      this.cardCCVElement.focus();
                    }
                    if (
                      event.target.value.length === 2 &&
                      !this.state.card_expiry.includes("/")
                    ) {
                      this.setState({
                        card_expiry:
                          event.target.value.replace(/[^\d\/]/g, "") + "/",
                        card_expiry_valid: event.target.value.length === 5,
                      });
                    } else {
                      this.setState({
                        card_expiry: event.target.value.replace(/[^\d\/]/g, ""),
                        card_expiry_valid: event.target.value.length === 5,
                      });
                    }
                  }}
                ></Form.Control>
              </Col>

              {/* Card CCV number input field logic */}

              <Col className="col-3">
                <Form.Control
                  id="card_ccv"
                  value={this.state.card_ccv}
                  className={`text-center ${
                    this.state.card_ccv_valid ||
                    this.state.card_ccv_valid === undefined
                      ? "vrf--card-number"
                      : "vrf--card-number__invalid"
                  }`}
                  maxLength={3}
                  placeholder="CCV"
                  onChange={(event) => {
                    this.setState({
                      card_ccv: removeAllButNumbers(event.target.value),
                      card_ccv_valid: event.target.value.length === 3,
                    });
                  }}
                ></Form.Control>
              </Col>
            </Row>
          </div>
        </Container>

        <Container
          style={{
            opacity: this.state.submitted ? 0.2 : 1,
            pointerEvents: this.state.submitted ? "none" : "auto",
          }}
        >
          <div className="vrf--verify-button">
            <Row className="justify-content-center">
              <Button
                onClick={() => this.submitCard()}
                variant="outline-primary"
              >
                Verify!
              </Button>
            </Row>
          </div>
        </Container>
      </>
    );
  }
}

export default App;
