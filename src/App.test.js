import { cleanup, render, screen } from "@testing-library/react";
import App from "./App";

test("renders learn react link", () => {
  render(<App />);
  const linkElement = screen.getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
  afterEach(cleanup);
  it("adds '/' to expiry input after 2 digits", () => {
    render(<App />);
 
    const inputEl = screen.getByTestId("expiry-input");
  
    expect(inputEl).toBeInTheDocument();
    expect(inputEl).toHaveAttribute("type", "email");
  })  
});

